Required Legal Notice

Modlet for 7 Days To Die Game.

All Models in this modlet are subject to creative commons licence.
https://creativecommons.org/licenses/by/4.0/
Model Credits : If Known
The model is set up with appropriate lighting shader and textures in unity for 7 Days to Die game.
A big thanks to all the modellers who release work for free usage !
A big Thanks to Ragsy for The rigging and to the ATeam for sponsoring This modlet